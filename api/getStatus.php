<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once '../includes/auth/db_connect.php';
include_once '../includes/auth/functions.php';
include_once '../includes/php/productHandler.php';
include_once '../includes/php/orgHandler.php';

$output = array();

foreach (getAllOrganisations() as $org){
    if($org['id'] == 1) continue;
    if(!isActiveOrg($org['id'])) continue;
    if(isFantasy($org['id'])) continue;
    if(!showOpen($org['id'])) continue;
    $lastActiveReport = $org['lastActiveReport'];
    if(isPartner($org['id'])){
        $partner = "Partner";
    }else{
        $partner = "Intern";
    }
    if($org['lastActiveReportBy'] == 0){
        $medewerker = "Onbekend";
    }else{
        $medewerker = getUserInfo($org['lastActiveReportBy'])['name'];
    }
    if((time() - (60 * 5)) < $lastActiveReport){
        $output[$org['name'] . " (".$org['location'].")"] = array("open",  ' ' . $org['icon'], $partner, $medewerker);
    }else{
        $output[$org['name'] . " (".$org['location'].")"] = array("closed",  ' ' . $org['icon'], $partner, "Onbekend");
    }
}

print json_encode($output, JSON_UNESCAPED_UNICODE);
