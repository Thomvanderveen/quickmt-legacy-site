<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once '../includes/auth/db_connect.php';
include_once '../includes/auth/functions.php';
include_once '../includes/php/productHandler.php';
include_once '../includes/php/orgHandler.php';
include_once '../includes/php/permHandler.php';
include_once '../includes/php/userInfo.php';

if (isset($_POST['userid'],$_POST['username'],$_POST['login_string'],$_POST['org'])) {
    if(login_checkWithVariables($mysqli, $_POST['userid'],$_POST['username'],$_POST['login_string'])){
        if(hasAccess($_POST['org'], $_POST['userid'])){
            if(orgHasPage($_POST['org'], 1) && hasPerms($_POST['org'], $_POST['userid'], "page.verkoop")){
                setActive($_POST['org'],$_POST['userid']);
                setUserActive($_POST['org'], $_POST['userid']);
            }
        }
    }
}
