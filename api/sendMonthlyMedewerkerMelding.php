<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include_once '../includes/auth/db_connect.php';
include_once '../includes/auth/functions.php';
include_once '../includes/php/productHandler.php';
include_once '../includes/php/orgHandler.php';
include_once '../includes/php/permHandler.php';
include_once '../includes/php/userInfo.php';

if(date("d") == 28){
    $meldingGroupID = 'MonthlyEmployee::' . rand(111111111,999999999);
    foreach (getAllUsers() as $user){
        if(isAdmin($user['id'])) continue;
        if($user['active'] == 0) continue;
        $send = false;
        foreach (getAllOrganisations() as $org){
            if($org['id'] == 1) continue;
            if(!isActiveOrg($org['id'])) continue;
            if($org['fantasy'] == 1) continue;
            if($org['partner'] == 1) continue;
            if(hasPerms($org['id'], $user['id'], "page.leaderboard.medwVanDeMaand")){
                $send = true;
            }
        }
        if($send){
            createMelding($user['id'],0,'high', 'fa-exclamation-triangle', 'Heey Eigenaar/Manager even een herinnering van mij (de developer). Denken jullie aan het kiezen van een medewerker van de maand ;) <br><br><br>Dit bericht is automatisch gegenereerd', $meldingGroupID);
        }
    }
}

if(date("w") == 3){
    $meldingGroupID = 'WeeklyTalk::' . rand(111111111,999999999);
    foreach (getAllUsers() as $user){
        if(isAdmin($user['id'])) continue;
        if($user['active'] == 0) continue;
        $send = false;
        foreach (getAllOrganisations() as $org){
            if($org['id'] == 1) continue;
            if(!isActiveOrg($org['id'])) continue;
            if($org['fantasy'] == 1) continue;
            if($org['partner'] == 1) continue;
            if(hasPerms($org['id'], $user['id'], "loon.percentage.125")){
                $send = true;
            }
        }
        if($send){
            createMelding($user['id'],0,'high', 'fa-exclamation-triangle', 'Heey Eigenaar/Manager even een herinnering van mij (de developer). Denk je aan de wekelijkse vergadering morgen? <br><br><br>Dit bericht is automatisch gegenereerd', $meldingGroupID);
        }
    }
}
if(date("w") == 6){
    $meldingGroupID = 'VoorraadVideo::' . rand(111111111,999999999);
    foreach (getAllUsers() as $user){
        if(isAdmin($user['id'])) continue;
        if($user['active'] == 0) continue;
        $send = false;
        foreach (getAllOrganisations() as $org){
            if($org['id'] == 1) continue;
            if(!isActiveOrg($org['id'])) continue;
            if($org['fantasy'] == 1) continue;
            if($org['partner'] == 1) continue;
            if(hasPerms($org['id'], $user['id'], "page.overzicht.voorraad.manage")){
                $send = true;
            }
        }
        if($send){
            createMelding($user['id'],0,'high', 'fa-exclamation-triangle', 'Heey Eigenaar/Manager/Inspecteur even een herinnering van mij (de developer). Denk je aan de wekelijkse voorraad video (die je voor zondag moet sturen) <br><br><br>Dit bericht is automatisch gegenereerd', $meldingGroupID);
        }
    }
}

if(date("w") == 5){
    $meldingGroupID = 'DiscordBackup::' . rand(111111111,999999999);
    foreach (getAllUsers() as $user){
        if(isAdmin($user['id'])) continue;
        if($user['active'] == 0) continue;
        $send = false;
        foreach (getAllOrganisations() as $org){
            if($org['id'] == 1) continue;
            if(!isActiveOrg($org['id'])) continue;
            if($org['fantasy'] == 1) continue;
            if($org['partner'] == 1) continue;
            if(hasPerms($org['id'], $user['id'], "loon.percentage.125")){
                $send = true;
            }
        }
        if($send){
            createMelding($user['id'],0,'high', 'fa-exclamation-triangle', 'Heey Eigenaar/Manager even een herinnering van mij (de developer). Denk je aan het backuppen van de discord server? <br><br><br>Dit bericht is automatisch gegenereerd', $meldingGroupID);
        }
    }
}



