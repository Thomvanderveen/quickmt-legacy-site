<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();

include_once 'includes/auth/db_connect.php';
include_once 'includes/auth/functions.php';

sec_session_start();

$_VERSION = "1.2";

require_once __DIR__ . '/errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

require_once 'includes/php/activePage.php';
require_once 'includes/php/orgHandler.php';
include_once 'includes/php/pageLoader.php';
include_once 'includes/php/userInfo.php';
include_once 'includes/php/logHandler.php';
include_once 'includes/php/bestellingHandler.php';
include_once 'includes/php/meldingHandler.php';
include_once 'includes/php/permHandler.php';
include_once 'includes/php/productHandler.php';
include_once 'includes/php/verkoopHandler.php';
include_once 'includes/php/voorraadHandler.php';
include_once 'includes/php/omzetHandler.php';
include_once 'includes/php/loonHandler.php';
include_once 'includes/php/noteHandler.php';
include_once 'includes/php/languageHandler.php';
include_once 'includes/php/mutatieHandler.php';
include_once 'includes/php/bonusHandler.php';
include_once 'includes/php/planningHandler.php';
include_once 'includes/php/sollicitatieHandler.php';
include_once 'includes/php/afwezigheidHandler.php';
include_once 'includes/php/levelHandler.php';
include_once 'includes/php/topMedwHandler.php';
include_once 'includes/php/belastingHandler.php';

$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(substr($url, -1) != "/"){
    $url .= "/";
    ob_end_clean();
    header('Location: '.$url);
    exit();
}

$request = explode('/', trim($_SERVER['REQUEST_URI'],'/'));

$pagina = getActivePage($request);

require_once './includes/modules/main_header.php';

if(isset($request[0]) && $request[0] == 'solliciteren') {
    require_once './includes/modules/sollicitatie.php';
}elseif(login_check($mysqli) != true){
    require_once './includes/modules/auth_login.php';
}elseif(!isset($_SESSION['org']) || $_SESSION['org'] == -1){
    require_once './includes/modules/auth_bedrijf.php';
}elseif(!hasAccess($_SESSION['org'], $_SESSION['user_id'])) {
    $_SESSION['org'] = -1;
    header('Location: /');
    exit();
}elseif(!userIsALlowedToBypassChrome($_SESSION['user_id']) && !(preg_match('/(Chrome|CriOS)\//i',$_SERVER['HTTP_USER_AGENT'])  && !preg_match('/(Aviator|ChromePlus|coc_|Dragon|Edge|Flock|Iron|Kinza|Maxthon|MxNitro|Nichrome|OPR|Perk|Rockmelt|Seznam|Sleipnir|Spark|UBrowser|Vivaldi|WebExplorer|YaBrowser)/i',$_SERVER['HTTP_USER_AGENT']))){
    require_once './includes/modules/browser_blocked.php';
}elseif(!isActiveOrg($_SESSION['org'])) {
    $_SESSION['org'] = -1;
    header('Location: /');
    exit();
}elseif(isset($request[0]) && $request[0] == "uitloggen") {
    header('Location: /includes/auth/process_logout.php');
    exit();
}elseif(isset($request[0]) && $request[0] == "uitloggen_org") {
    $_SESSION['org'] = -1;
    header('Location: /');
    exit();
}elseif(isset($request[0]) && $request[0] == "admin" && !isAdmin($_SESSION['user_id'])) {
    header('Location: /home/');
    exit();
}else{
    echo '<script>localStorage.setItem(\'username\', \''.getUserInfo($_SESSION['user_id'])['username'].'\');</script>';
    echo '<script>localStorage.setItem(\'token\', \''.getUserInfo($_SESSION['user_id'])['code'].'\');</script>';

    if(userAllowedToCheckLevel($_SESSION['user_id'])){
        berekenLevel($_SESSION['user_id']);
    }

    foreach (getAllUsers() as $user){
        if(isAdmin($user['id'])) continue;
        if($user['active'] == 0) continue;
        if(userAllowedToUpdateMedwVanDeMaandPunten($user['id'])){
            berekenMedwVanDeMaandPunten($user['id']);
        }
    }

    print '<body id="page-top">';
    print '<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FC4RM3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>';
    print '<div id="wrapper">';
    require_once './includes/modules/main_navbar.php';
    print '<div id="content-wrapper" class="d-flex flex-column">';
    print '<div id="content">';
    require_once './includes/modules/main_topbar.php';
    print '<div class="container-fluid">';
    load($request);
    print '</div>';
    print '</div>';
    require_once './includes/modules/main_footer_page.php';
    print '</div>';
    print '</div>';
    print '<a class="scroll-to-top rounded" href="#page-top"> <i class="fas fa-angle-up"></i></a>';
    require_once './includes/modules/modal_auth_logout.php';
    require_once './includes/modules/modal_auth_bedrijf.php';
    require_once './includes/modules/modal_admin_organisatie.php';
    require_once './includes/modules/modal_admin_gebruiker.php';
    require_once './includes/modules/modal_org_calculate_loon.php';
    require_once './includes/modules/modal_notitie_toevoegen.php';
    require_once './includes/modules/modal_org_admin_users.php';
    require_once './includes/modules/modal_org_createProduct.php';
}
require_once './includes/modules/main_footer.php';
ob_end_flush();
