<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function shutdownErrorFunction($session){
    $error = error_get_last();
    if ($error['type'] === E_ERROR || $error['type'] === E_PARSE  || $error['type'] === E_CORE_ERROR ) {
        ob_clean();
        require __DIR__ . '/includes/modules/main_header.php';
        require_once __DIR__ . '/includes/modules/main_error.php';
        if(isset($session['username'])){
            printMainError($error, true);
        }else{
            printMainError($error, false);
        }
        require_once __DIR__ .  '/includes/modules/main_footer.php';
        ob_end_flush();
    }
}