<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getOngelezenMeldingen($userid){
    global $mysqli;
    $gelezen = 0;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE user_id = ? AND gelezen = ?")) {
        $stmt->bind_param('ii', $userid, $gelezen);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getMelding($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function getAlleMeldingen($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE user_id = ? ORDER BY gelezen,id DESC")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getVerstuurdeMeldingen($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE created_by = ? GROUP BY melding_group_id ORDER BY id DESC")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getMeldingenMetZelfdeGroupID($groupid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE melding_group_id = ?")) {
        $stmt->bind_param('s', $groupid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getHoeveelheidDezelfdeVerstuurdeMelding($groupid){
    global $mysqli;
    $stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE melding_group_id = ?");
    $stmt->bind_param('s', $groupid);
    $stmt->execute();
    $stmt->store_result();
    return $stmt->num_rows;
}

function getAantalOngelezenMelding($userid){
    global $mysqli;
    $gelezen = 0;
    $stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE user_id = ? AND gelezen = ?");
    $stmt->bind_param('ii', $userid,$gelezen);
    $stmt->execute();
    $stmt->store_result();
    return $stmt->num_rows;
}

function createMelding($user_id, $creator_id, $prioriteit, $icon, $message, $group_melding_id){
    global $mysqli;
    $now = time();
    if ($stmt = $mysqli->prepare("INSERT INTO meldingen (user_id, created_by, created_on, prioriteit, icon, message, melding_group_id) VALUES (?,?,?,?,?,?,?)")) {
        $stmt->bind_param('iisssss', $user_id, $creator_id, $now, $prioriteit, $icon, $message, $group_melding_id);
        $stmt->execute();
        $stmt->store_result();
    }
}

function createMelding_with_mysqli($mysqli,$user_id, $creator_id, $prioriteit, $icon, $message, $group_melding_id){
    $now = time();
    if ($stmt = $mysqli->prepare("INSERT INTO meldingen (user_id, created_by, created_on, prioriteit, icon, message, melding_group_id) VALUES (?,?,?,?,?,?,?)")) {
        $stmt->bind_param('iisssss', $user_id, $creator_id, $now, $prioriteit, $icon, $message, $group_melding_id);
        $stmt->execute();
        $stmt->store_result();
    }
}

function meldingExists($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
}

function userCanReadMelding($user,$id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM meldingen WHERE id = ? AND user_id = ? LIMIT 1")) {
        $stmt->bind_param('ii', $id, $user);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows != 1) {
            return false;
        }else{
            return true;
        }
    }
    return false;
}

function setMeldingAlsGelezen($id){
    global $mysqli;
    $gelezen = 1;
    if ($stmt = $mysqli->prepare("UPDATE meldingen SET gelezen = ? WHERE id = ?")) {
        $stmt->bind_param('ii', $gelezen, $id);
        $stmt->execute();
        $stmt->store_result();
    }
}
