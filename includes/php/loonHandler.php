<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function addLoon($org, $user, $total, $time, $managerid){
    $total = round($total,2);
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO lonen (org_id, user_id, total, time, calculatedBy) VALUES (?,?,?,?, ?)")) {
        $stmt->bind_param('iidsi', $org , $user, $total,$time,$managerid);
        $stmt->execute();
    }
}

function getLoonFromUser($org, $user){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM lonen WHERE org_id = ? AND user_id = ?")) {
        $stmt->bind_param('ii', $org, $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return 0;
}

function getAllLastLoon($org, $time){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM lonen WHERE org_id = ? AND time = ?")) {
        $stmt->bind_param('is', $org, $time);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return 0;
}

function getAllLastLoonFromUser($org, $time, $userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM lonen WHERE org_id = ? AND time = ? AND user_id = ?")) {
        $stmt->bind_param('isi', $org, $time,$userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return 0;
}

function getAllLoonFromUser($org, $userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM lonen WHERE org_id = ? AND user_id = ?")) {
        $stmt->bind_param('ii', $org, $userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return 0;
}

function getLoonPercentage($org, $user){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM userLoonPercentage WHERE org_id = ? AND user_id = ?")) {
        $stmt->bind_param('ii', $org, $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0]['loonpercentage'];
    }
    return 0;
}

function setLoonPercentageForPartners($org,$user,$percentage){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM userLoonPercentage WHERE org_id = ? AND user_id = ?")) {
        $stmt->bind_param('ii', $org , $user);
        $stmt->execute();
    }
    if ($stmt = $mysqli->prepare("INSERT INTO userLoonPercentage (org_id, user_id, loonpercentage) VALUES (?,?,?)")) {
        $stmt->bind_param('iid', $org , $user, $percentage);
        $stmt->execute();
    }
}
