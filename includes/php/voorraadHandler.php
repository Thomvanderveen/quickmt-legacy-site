<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getVoorraad($org, $productid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM voorraad WHERE org_id = ? AND product_id = ?")) {
        $stmt->bind_param('ii', $org, $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        if(isset($allRows[0]['ammount'])){
            return $allRows[0]['ammount'];
        }else{
            return 0;
        }
    }
    return 0;
}

function voorraadIsFromNewProduct($org,$product){
    return true;
}

function updateVoorraadAfterVerkoop($org, $productid, $aantal){
    global $mysqli;
    if(voorraadIsFromNewProduct($org,$productid)){
        createVoorraad($org,$productid);
    }
    if ($stmt = $mysqli->prepare("UPDATE voorraad SET ammount = ammount - ? WHERE org_id = ? AND product_id = ?")) {
        $stmt->bind_param('iii', $aantal , $org, $productid);
        $stmt->execute();
    }
}

function addVoorraad($org, $productid, $aantal){
    global $mysqli;
    if(voorraadIsFromNewProduct($org,$productid)){
        createVoorraad($org,$productid);
    }
    if ($stmt = $mysqli->prepare("UPDATE voorraad SET ammount = ammount + ? WHERE org_id = ? AND product_id = ?")) {
        $stmt->bind_param('iii', $aantal , $org, $productid);
        $stmt->execute();
    }
}

function removeVoorraad($org, $productid, $aantal){
    global $mysqli;
    if(voorraadIsFromNewProduct($org,$productid)){
        createVoorraad($org,$productid);
    }
    if ($stmt = $mysqli->prepare("UPDATE voorraad SET ammount = ammount - ? WHERE org_id = ? AND product_id = ?")) {
        $stmt->bind_param('iii', $aantal , $org, $productid);
        $stmt->execute();
    }
}

function createVoorraad($org, $productid){
    $ammount = 0;
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO voorraad (org_id, product_id, ammount) VALUES (?,?,?)")) {
        $stmt->bind_param('iii', $org , $productid, $ammount);
        $stmt->execute();
    }
}

function deleteVoorraad($org, $productid){
    $ammount = 0;
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM voorraad WHERE org_id = ? AND product_id = ?")) {
        $stmt->bind_param('ii', $org , $productid);
        $stmt->execute();
    }
}

function voorraadIsChecked($org){
    $date = strtotime(date("Y/m/d", time()));
    $orgInfo = getOrganisation($org);
    if($orgInfo['lastVoorraadCheckedOn'] > $date){
        return true;
    }
    return false;
}

function voorraadCheckedBy($org){
    $orgInfo = getOrganisation($org);
    $userid = $orgInfo['lastVoorraadCheckedBy'];
    return getUserInfo($userid)['name'];
}

function voorraadChecked($userid, $org){
    setLastVoorraadChecked($org, $userid);
}