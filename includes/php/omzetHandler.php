<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getTotaleOmzet($minuten, $orgid, $type){
    $time = time();
    $time -= ($minuten * 60);
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE time > ? AND org_id = ? AND type = ?")) {
        $stmt->bind_param('iis', $time,$orgid, $type);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}

function getTotaleOmzetAltijd($orgid, $type){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE org_id = ? AND type = ?")) {
        $stmt->bind_param('is', $orgid, $type);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}

function getAllUserOmzetAltijd($orgid, $userid, $type){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE org_id = ? AND user_id = ? AND type = ?")) {
        $stmt->bind_param('iis', $orgid, $userid, $type);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}

function getAllUserOmzetFromAllOrgs($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE user_id = ?")) {
        $stmt->bind_param('i',  $userid);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return 0;
        }
    }
    return 0;
}

function getAllUserOmzetFromAllOrgsLast30Days($userid){
    global $mysqli;
    $time = time() - (60*60*24*30);
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE user_id = ? AND time > ?")) {
        $stmt->bind_param('ii',  $userid,$time);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return 0;
        }
    }
    return 0;
}

function getAllOmzetFromAllOrgsLast30Days(){
    global $mysqli;
    $time = time() - (60*60*24*30);
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE time > ?")) {
        $stmt->bind_param('i',  $time);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return 0;
        }
    }
    return 0;
}


function getAllUserOmzet($minuten, $orgid, $userid, $type){
    $time = time();
    $time -= ($minuten * 60);
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE time > ? AND org_id = ? AND user_id = ? AND type = ?")) {
        $stmt->bind_param('iiis', $time,$orgid, $userid, $type);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}

function getOmzetSinceLastoonRest($orgid,$userid,$time, $type){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE time > ? AND org_id = ? AND user_id = ? AND type = ?")) {
        $stmt->bind_param('iiis', $time,$orgid, $userid, $type);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}

function getOmzetTotalPerUserSinceLastoonRest($orgid,$userid,$time){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE time > ? AND org_id = ? AND user_id = ?")) {
        $stmt->bind_param('iii', $time,$orgid, $userid);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}

function getOmzetTotalSinceLastoonRest($orgid,$time, $type){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT SUM(price) FROM verkopen WHERE time > ? AND org_id = ? AND type = ?")) {
        $stmt->bind_param('iis', $time,$orgid, $type);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($total);
        $stmt->fetch();
        if(isset($total)){
            return $total;
        }elseif($total == NULL) {
            return 0;
        }else{
            return NULL;
        }
    }
    return 0;
}
