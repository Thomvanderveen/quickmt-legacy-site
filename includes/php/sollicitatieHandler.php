<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function addSollicitatie($functie,$username,$name,$tutorial,$online_time,$start_date,$online_time_day,$online_time_day_shop,$level,$city,$leeftijd,$contact,$job,$vog,$werkervaring,$motivatie,$contact_info,$job_text,$comp){
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO sollicitatie (functie, bedrijf, username, name, tutorial, online_time, start_date, online_time_day, online_time_day_comp, level, city_registered, leeftijd, contact_social, job, job_title, werkervaring, VOG, motivatie, contact) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $stmt->bind_param('sississssssssisssss', $functie,$comp,$username,$name,$tutorial,$online_time,$start_date,$online_time_day,$online_time_day_shop,$level,$city,$leeftijd,$contact,$job,$job_text,$werkervaring,$vog,$motivatie,$contact_info);
    $stmt->execute();
}

function getSollicitaties(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM sollicitatie ORDER BY id DESC")) {
        $stmt->bind_param('is', $org, $type);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getSollicitatie($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM sollicitatie WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function sollicitatieExists($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM sollicitatie WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }else{
            return false;
        }
    }
    return false;
}

function updateSollicitatieStatus($id, $status,$userid){
    global $mysqli;
    $now = time();
    if ($stmt = $mysqli->prepare("UPDATE sollicitatie SET status = ?,handledBy = ?, handledOn = ? WHERE id = ?")) {
        $stmt->bind_param('siii', $status, $userid,$now,$id);
        $stmt->execute();
        $stmt->store_result();
    }
}

function printSollicitatie($sollicitatie, $isPending){ ?>
    <div class="card shadow mb-4">
        <a href="#sollicitatie_<?php print $sollicitatie['id'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sollicitatie_<?php print $sollicitatie['id'] ?>">
            <h6 class="m-0 font-weight-bold text-primary"><?php print $sollicitatie['username'] ?></h6>
        </a>
    <div class="collapse hide" id="sollicitatie_<?php print $sollicitatie['id'] ?>">
        <div class="card-body">
            <div class="row">
                <?php if($isPending){ ?>
                    <a class="btn btn-success text-white" onclick="window.location.href='/org/overzicht/sollicitatie/<?php print $sollicitatie['id']; ?>/archive/'">Archiveren</a>
                <?php } ?>
                <br>
                <div class="table">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                        <?php if(!$isPending){ ?>
                            <td class="font-weight-bold" style="width: 25%">Verwerkt Door</td>
                            <?php if(userExistsByUserID($sollicitatie['handledBy'])){ ?>
                                <td><?php print getUserInfo($sollicitatie['handledBy'])['name'] ?></td>
                            <?php }else{ ?>
                                <td>Systeem</td>
                            <?php } ?>
                        <?php } ?>
                        </tr>
                        <tr>
                            <?php if(!$isPending){ ?>
                                <td class="font-weight-bold" style="width: 25%">Verwerkt Op</td>
                            <td> <?php print date("d-m-Y H:i:s", $sollicitatie['handledOn']); ?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Functie</td>
                            <td><?php print $sollicitatie['functie'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Bedrijf</td>
                            <?php if(orgExistsByID($sollicitatie['bedrijf'])){ ?>
                                <td><?php print getOrganisation($sollicitatie['bedrijf'])['name']; ?> (<?php print getOrganisation($sollicitatie['bedrijf'])['location']; ?>)</td>
                            <?php }else{ ?>
                                <td>Geen voorkeur</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Username</td>
                            <td><?php print $sollicitatie['username'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Naam</td>
                            <td><?php print $sollicitatie['name'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Tutorial Afgerond</td>
                            <?php if($sollicitatie['tutorial'] == 1){ ?>
                                <td>Ja</td>
                            <?php }else{ ?>
                                <td>Nee</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Totale Online Time</td>
                            <td><?php print $sollicitatie['online_time'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Start Datum</td>
                            <td><?php print $sollicitatie['start_date'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Tijd Online (Per Dag)</td>
                            <td><?php print $sollicitatie['online_time_day'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Tijd In Het Bedrijf (Per Dag)</td>
                            <td><?php print $sollicitatie['online_time_day_comp'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Level</td>
                            <td><?php print $sollicitatie['level'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Ingeschreven Stad</td>
                            <td><?php print $sollicitatie['city_registered'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Leeftijd</td>
                            <td><?php print $sollicitatie['leeftijd'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Teamspeak/Discord</td>
                            <?php if($sollicitatie['contact_social'] == 'ja'){ ?>
                                <td>Ja</td>
                            <?php }else if($sollicitatie['contact_social'] == 'no_voice'){ ?>
                                <td>Ja (geen microfoon)</td>
                            <?php }else{ ?>
                                <td>Nee</td>
                            <?php } ?>
                        </tr>
                        <?php if($sollicitatie['job'] == 1){ ?>
                            <tr>
                                <td class="font-weight-bold" style="width: 25%">Nog een andere baan</td>
                                <td>Ja (<?php print $sollicitatie['job_title']; ?>)</td>
                            </tr>
                        <?php }else{ ?>
                            <tr>
                                <td class="font-weight-bold" style="width: 25%">Nog een andere baan</td>
                                <td>Nee</td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Werkervaring</td>
                            <td><?php print $sollicitatie['werkervaring'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">VOG</td>
                            <?php if($sollicitatie['VOG'] == "nee"){ ?>
                                <td>Negatief</td>
                            <?php }elseif($sollicitatie['VOG'] == "ja"){ ?>
                                <td>Positief</td>
                            <?php }else{ ?>
                                <td>Weet ik niet</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Motivatie</td>
                            <td><?php print $sollicitatie['motivatie'] ?></td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold" style="width: 25%">Contact</td>
                            <td><?php print $sollicitatie['contact'] ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php }