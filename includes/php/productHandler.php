<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getTotaalAantalProducten(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM products")) {
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function getAllProducts($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM products WHERE org_id = ?")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function orgHasProduct($orgID, $productid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM products WHERE id = ? AND org_id = ? LIMIT 1")) {
        $stmt->bind_param('ii', $productid,$orgID);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 1){
            return true;
        }
    }
    return false;
}


function getAllProductsIDAndPrice($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT id,price FROM products WHERE org_id = ?")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getProductByID($productid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM products WHERE id = ?")) {
        $stmt->bind_param('i' , $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function createProduct($org,$name,$icon,$price){
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO products (org_id, name,icon,price) VALUES (?,?,?,?)");
    $stmt->bind_param('issd', $org,$name, $icon,$price);
    $stmt->execute();
}

function updateProduct($id,$name,$icon,$price,$isLimited,$limit){
    global $mysqli;
    $stmt = $mysqli->prepare("UPDATE products SET name = ?, icon = ? ,price = ?, isLimited = ?, `limit` = ? WHERE id = ?");
    $stmt->bind_param('ssdiii', $name, $icon,$price,$isLimited,$limit,$id);
    $stmt->execute();
}

function deleteProduct($productid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("UPDATE products SET org_id = 0 WHERE id = ?")) {
        $stmt->bind_param('i', $productid);
        $stmt->execute();
    }
}

function productIsDailyLimited($productid){
    global $mysqli;
    $limited = 1;
    if ($stmt = $mysqli->prepare("SELECT * FROM products WHERE id = ? AND isLimited = ? LIMIT 1")) {
        $stmt->bind_param('ii', $productid,$limited);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 1){
            return true;
        }
    }
    return false;
}

function getProductLimit($productid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT `limit` FROM products WHERE id = ?")) {
        $stmt->bind_param('i', $productid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0]['limit'];
    }
    return 0;
}