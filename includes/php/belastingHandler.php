<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function partnerHasOpenPayment($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM belasting WHERE org_id = ? AND accepted_by = 0 LIMIT 1")) {
        $stmt->bind_param('i', $org);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows == 1){
            return true;
        }
    }
    return false;
}

function getOpenPayment($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM belasting WHERE org_id = ? AND accepted_by = 0 ")) {
        $stmt->bind_param('i' , $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows[0];
    }
    return array();
}

function createPayment($org,$price){
    global $mysqli;
    $stmt = $mysqli->prepare("INSERT INTO belasting (org_id, price,created_on) VALUES (?,?,?)");
    $stmt->bind_param('idi', $org,$price,time());
    $stmt->execute();
}

function paymentPayed($id,$userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("UPDATE belasting SET accepted_by = ? WHERE id = ?")) {
        $stmt->bind_param('ii', $userid,$id);
        $stmt->execute();
    }
}