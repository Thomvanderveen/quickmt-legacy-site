<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function addLog($userid, $value){
    global $mysqli;
    $value = strip_tags($value);
    $now = time();
    $ip = getUserIp();
    $stmt = $mysqli->prepare("INSERT INTO logs (id,time,ip,value) VALUES (?,?,?,?)");
    $stmt->bind_param('isss', $userid, $now, $ip,$value);
    $stmt->execute();
}

function getLogs($userid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM logs WHERE id = ? ORDER BY time DESC LIMIT 1000")) {
        $stmt->bind_param('i', $userid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function totaalAantalLogs($minuten){
    $time = time();
    $time -= ($minuten * 60);
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT time FROM logs WHERE time > ?")) {
        $stmt->bind_param('i', $time);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}