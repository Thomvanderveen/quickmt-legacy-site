<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function addVerkoop($userid, $org, $klant, $type, $producten, $price){
    global $mysqli;
    $now = time();
    $producten = json_encode($producten);
    if ($stmt = $mysqli->prepare("INSERT INTO verkopen (user_id, org_id, klant, type, price, producten,time) VALUES (?,?,?,?,?,?,?)")) {
        $stmt->bind_param('iissdss', $userid , $org,$klant,$type,$price,$producten, $now);
        $stmt->execute();
    }
}

function getTotaalAantalVerkopen(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen")) {
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows;
    }
    return 0;
}

function deleteVerkoop($org, $id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("DELETE FROM verkopen WHERE org_id = ? AND id = ?")) {
        $stmt->bind_param('ii', $org,$id);
        $stmt->execute();
        $stmt->store_result();
    }
}

function getAllVerkopen($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen WHERE org_id = ? ORDER BY time DESC")) {
        $stmt->bind_param('i', $org );
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}
function getAllVerkopenAfterDate($org,$time){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen WHERE org_id = ? AND time > ? ORDER BY time DESC")) {
        $stmt->bind_param('ii', $org,$time);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}


function getAllVerkopenFromUserToday($org,$name){
    $time = strtotime('today midnight');
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen WHERE org_id = ? AND klant = ? AND time > ?")) {
        $stmt->bind_param('iss', $org,$name,$time);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getAllVerkopenFromUser($org, $user){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen WHERE org_id = ? AND user_id = ? ORDER BY time DESC")) {
        $stmt->bind_param('ii', $org,$user);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getAllTotalVerkopenFromUser($user){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen WHERE user_id = ? ORDER BY time DESC")) {
        $stmt->bind_param('i',$user);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getVerkoop($org, $verkoopid){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM verkopen WHERE org_id = ? AND id = ?")) {
        $stmt->bind_param('ii', $org,$verkoopid);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}
