<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getAfwezigheden($userid,$org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM afwezigheid WHERE user_id = ? AND org_id = ? ORDER BY id DESC")) {
        $stmt->bind_param('ii', $userid, $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getAfwezigheid($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM afwezigheid WHERE id = ?")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function afwezigheidsMeldingExists($id){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM afwezigheid WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }else{
            return false;
        }
    }
    return false;
}

function createAfwezigheid($org, $user,$start,$end,$reden){
    global $mysqli;
    if ($stmt = $mysqli->prepare("INSERT INTO afwezigheid (org_id, user_id,start_date,end_date,reason) VALUE (?,?,?,?,?)")) {
        $stmt->bind_param('iiiis', $org,$user,$start,$end,$reden);
        $stmt->execute();
        $stmt->store_result();
    }
}

function updateAfwezigheidStatus($id,$status,$updated_by){
    global $mysqli;
    if ($stmt = $mysqli->prepare("UPDATE afwezigheid SET handled_by = ?, status = ? WHERE id = ?")) {
        $stmt->bind_param('isi', $updated_by,$status, $id);
        $stmt->execute();
        $stmt->store_result();
    }
}

function getAlleAfwezigheden($org){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM afwezigheid WHERE org_id = ? ORDER BY id DESC")) {
        $stmt->bind_param('i',  $org);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function getAllAfgewezenAfwezigheidLaatste100Dagen($userid){
    global $mysqli;
    $honderdDagenTerug = time() - (60*60*24*100);
    if ($stmt = $mysqli->prepare("SELECT * FROM afwezigheid WHERE user_id = ? AND status = 'geweigerd' AND end_date > ?")) {
        $stmt->bind_param('ii',  $userid,$honderdDagenTerug);
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function isInActive($org,$userid,$time){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT * FROM afwezigheid WHERE org_id = ? AND user_id = ? AND start_date < ? AND end_date > ? AND status = 'geaccepteerd' LIMIT 1")) {
        $stmt->bind_param('iiii', $org,$userid,$time,$time);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows == 1) {
            return true;
        }
    }
    return false;
}

function printAfwezigheid($afwezigheid, $isPending){
    if($afwezigheid['status'] == 'in behandeling'){
        $status = '<span class="font-weight-bold text-info">In Behandeling</span>';
    }elseif($afwezigheid['status'] == 'geaccepteerd'){
        $status = '<span class="font-weight-bold text-success">Geaccepteerd</span>';
    }else{
        $status = '<span class="font-weight-bold text-danger">Geweigerd</span>';
    }?>

    <div class="card shadow mb-4">
        <a href="#afwezigheid_<?php print $afwezigheid['id'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="afwezigheid_<?php print $afwezigheid['id'] ?>">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php print getUserInfo($afwezigheid['user_id'])['name']; ?> (<?php print date("d-m-Y", $afwezigheid['start_date']); ?> t/m <?php print date("d-m-Y", $afwezigheid['end_date']); ?>) - <?php print $status; ?>
            </h6>
        </a>
        <div class="collapse hide" id="afwezigheid_<?php print $afwezigheid['id'] ?>">
            <div class="card-body">
                <span class="h5">
                    <?php print $afwezigheid['reason']; ?>
                    <?php if($isPending){ ?>
                        <br>
                        <a class="btn btn-success text-white" onclick="window.location.href='/org/overzicht/afwezigheid/<?php print $afwezigheid['id']; ?>/accept/'">Accepteren</a>
                        <a class="btn btn-danger text-white" onclick="window.location.href='/org/overzicht/afwezigheid/<?php print $afwezigheid['id']; ?>/denied/'">Weigeren</a>
                    <?php } ?>
                </span>
            </div>
            <?php if($afwezigheid['handled_by'] !== 0 && $afwezigheid['status'] !== 'in behandeling'){ ?>
                <div class="card-footer">
                    <span class="h7 font-weight-bold">Verwerkt Door</span><br>
                    <span class="h9"><?php print getUserInfo($afwezigheid['handled_by'])['name']; ?></span><br>
                </div>
            <?php } ?>
        </div>
    </div>
<?php }
