<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

function getAllUserLevelInfo(){
    global $mysqli;
    if ($stmt = $mysqli->prepare("SELECT id,name, level FROM user LEFT JOIN userStats on id = user_id ORDER BY level DESC")) {
        $stmt->execute();
        $result = $stmt->get_result();
        $allRows = $result->fetch_all(MYSQLI_ASSOC);
        return $allRows;
    }
    return array();
}

function berekenLevel($userid)
{
    if (isAdmin($userid)) {
        setLevel($userid, 0);
        return;
    }

    $punten = 1;
    $userInfo = getUserInfo($userid);

    /*
     *  Als de gebruiker in de afgelopen 30 dagen zijn wachtwoord heeft gereset
     *  + 500
     */
    if ($userInfo['lastPasswordUpdate'] > (time() - (60 * 60 * 24 * 30))) {
        $punten += 500;
    }

    /*
    *  Als de gebruiker afgelopen 48 uur is ingelogd
    *  + 100
    */
    if ($userInfo['lastLevelCheck'] > (time() - (60 * 60 * 24 * 2))) {
        $punten += 100;
    }

    /*
    *  Punten berekeken aan de hand van het aantal organisaties waar hij/zij werkzaam is
    *  + (10/25/50/100/150)
    */
    $punten += PRIVATE_puntenAanDeHandVanBedrijven($userid);

    /*
     * Punten aan de hand van de role (Eigenaar,Manager,Inspecteur,Medewerker)
     * + (25,50,100,150) per bedrijf met een max van 250
     */
    $punten += PRIVATE_puntenAanDeHandVanFuncie($userid);

    /*
     * Punten aan de hand van totaal aantal verkopen
     * + (10 t/m 250)
     */
    $punten += PRIVATE_puntenAanDeHandVanAantalVerkopen($userid);

    /*
     * Punten aan de hand van de totale omzet
     * + (1 t/m 500)
     */
    $punten += PRIVATE_puntenAanDeHandVanOmzet($userid);

    /*
     * Minpunten aan de hand van afgewezen afwezigheid afgelopen 100 dagen
     * - (100,200,300)
     */
    $punten -= PRIVATE_minPuntenAanDeHandVanGeweigerdeAfwezigheid($userid);

    /*
     * Punten aan de hand van de kwartieren op de planning afgelopen week
     * + (0 t/m 840) 15 per kwartier
     */
    $punten += PRIVATE_puntenAanDeHandVanIngeplandeMomenten($userid);


    /*
     *  Level updaten
     */
    $level = PRIVATE_puntenNaarLevel($punten);
    setLevel($userid, $level);
    userLevelChecked($userid);
}

function PRIVATE_puntenNaarLevel($punten)
{
    switch ($punten) {
        case $punten > 3000:
            return 15;
            break;
        case $punten > 2600:
            return 14;
            break;
        case $punten > 2200:
            return 13;
            break;
        case $punten > 1750:
            return 12;
            break;
        case $punten > 1400:
            return 11;
            break;
        case $punten > 1100:
            return 10;
            break;
        case $punten > 750:
            return 9;
            break;
        case $punten > 500:
            return 8;
            break;
        case $punten > 350:
            return 7;
            break;
        case $punten > 250:
            return 6;
            break;
        case $punten > 175:
            return 5;
            break;
        case $punten > 100:
            return 4;
            break;
        case $punten > 50:
            return 3;
            break;
        case $punten > 25:
            return 2;
            break;
        default:
            return 1;
            break;
    }
}

function PRIVATE_puntenAanDeHandVanBedrijven($userid){
    $orgs = 0;
    foreach (getAllOrganisations() as $organisation) {
        if (!isActiveOrg($organisation['id'])) continue;
        if (hasAccess($organisation['id'], $userid)) {
            $orgs += 1;
        }
    }

    if ($orgs <= 0) {
        return 0;
    }

    switch ($orgs) {
        case $orgs == 1:
            return 10;
            break;
        case $orgs == 2:
            return 25;
            break;
        case $orgs == 3:
            return 50;
            break;
        case $orgs == 4:
            return 100;
        case $orgs >= 5:
            return 150;
    }
}

function PRIVATE_puntenAanDeHandVanFuncie($userid)
{
    $punten = 0;
    foreach (getAllOrganisations() as $organisation) {
        if (!isActiveOrg($organisation['id'])) continue;
        if(!hasAccess($organisation['id'], $userid)) continue;
        if (hasPerms($organisation['id'], $userid, "loon.percentage.0")) {
            $punten += 150;
        }elseif (hasPerms($organisation['id'], $userid, "loon.percentage.125")) {
            $punten += 100;
        }elseif (hasPerms($organisation['id'], $userid, "loon.percentage.11")) {
            $punten += 50;
        }else {
            $punten += 25;
        }
    }

    if($punten > 250){
        $punten = 250;
    }

    return $punten;
}

function PRIVATE_puntenAanDeHandVanAantalVerkopen($userid)
{
    $punten = floor(count(getAllTotalVerkopenFromUser($userid)) / 10);
    if($punten > 250){
        $punten = 250;
    }
    return $punten;
}

function PRIVATE_puntenAanDeHandVanOmzet($userid)
{
    $punten = floor(getAllUserOmzetFromAllOrgs($userid)/ 1000);
    if($punten > 500){
        $punten = 500;
    }
    return $punten;
}

function PRIVATE_minPuntenAanDeHandVanGeweigerdeAfwezigheid($userid)
{
    $minPunten = count(getAllAfgewezenAfwezigheidLaatste100Dagen($userid));
    if($minPunten <= 0){
        return 0;
    }
    if($minPunten >= 4){
        $minPunten = 3;
    }
    switch ($minPunten){
        case 1:
            return 100;
            break;
        case 2:
            return 200;
            break;
        case 3:
            return 300;
            break;
    }
}

function PRIVATE_puntenAanDeHandVanIngeplandeMomenten($userid)
{
    $momenten = getAantalPlanningenAfgelopenWeek($userid);
    if($momenten > 56){
        $momenten = 56;
    }
    return $momenten * 15;
}