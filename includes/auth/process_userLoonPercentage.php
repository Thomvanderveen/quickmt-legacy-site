<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/userInfo.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/loonHandler.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if (isset($_POST['percentage'], $_POST['user_org'])) {

    $percentage = ltrim(rtrim(strip_tags($_POST['percentage']))) / 100;
    $user_org = ltrim(rtrim(strip_tags($_POST['user_org'])));
    $user = explode("-",$user_org)[0];
    $org = explode("-",$user_org)[1];

    if(!userExistsByUserID($user)){
        header('Location: /org/admin/users/'.$user.'/');
        exit();
    }

    if(!orgExistsByID($org)){
        header('Location: /org/admin/users/'.$user.'/');
        exit();
    }

    if(!isActiveOrg($org)){
        header('Location: /org/admin/users/'.$user.'/');
        exit();
    }

    if(!hasAccess($org,$user)){
        header('Location: /org/admin/users/'.$user.'/');
        exit();
    }

    setLoonPercentageForPartners($org, $user, $percentage);

    $org = getOrganisation($org);

    addLog($_SESSION['user_id'], "Succesvolle het loonpercentage van de organisatie ". $org["name"] . " (".$org['location'].") van de gebruiker ".$user." aangepast naar " . $percentage);
    header('Location: /org/admin/users/'.$user.'/');
    exit();
}
header('Location: /org/admin/users/'.$user.'/');
exit();



