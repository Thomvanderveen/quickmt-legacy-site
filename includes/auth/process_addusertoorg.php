<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';
include_once '../php/permHandler.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['useridtoorg'])){
    $userid = $_POST['useridtoorg'];

    if($userid == ""){
        header('Location: /org/admin/users/empty_user/');
        exit();
    }

    if(!userExistsByUserID($userid)){
        header('Location: /org/admin/users/unknown_user/');
        exit();
    }

    if($userid == $_SESSION['user_id']){
        header('Location: /org/admin/users/you/');
        exit();
    }

    if(isAdmin($userid)){
        header('Location: /org/admin/users/user_is_admin/');
        exit();
    }

    if(!hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users.add")){
        header('Location: /org/admin/users/no_perms/');
        exit();
    }

    if(hasAccess($_SESSION['org'], $userid)){
        header('Location: /org/admin/users/already_added/');
        exit();
    }

    if(!isActiveUser($userid)){
        header('Location: /org/admin/users/unknown_user/');
        exit();
    }

    addUserToOrg($_SESSION['org'], $userid);

    $user = getUserInfo($userid);

    $org = getOrganisation($_SESSION['org']);

    addLog($_SESSION['user_id'], "Succesvolle de gebruiker ".$user['username']." - (".$user['name'].") toegevoegd aan de organisatie ". $org['name']. " (".$org['location'].")");

    header('Location: /org/admin/users/user_added/');
    exit();
}

header('Location: /org/admin/users/');
exit();