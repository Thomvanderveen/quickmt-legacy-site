<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';
include_once '../php/productHandler.php';
include_once '../php/permHandler.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['username'], $_POST['price'])){
    $username = ltrim(rtrim(strip_tags($_POST['username'])));
    if(isset($_POST['icon'])){
        $icon = ltrim(rtrim(strip_tags($_POST['icon'])));
    }else{
        $icon = ":x:";
    }

    $price = $_POST['price'];

    if($username == ""){
        header('Location: /org/admin/products/error/');
        exit();
    }

    if($icon == ""){
        header('Location: /org/admin/products/error/');
        exit();
    }

    if(!is_numeric($price)){
        header('Location: /org/admin/products/error/');
        exit();
    }

    if(!hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.products.manage")){
        header('Location: /org/admin/products/error/');
        exit();
    }

    createProduct($_SESSION['org'], $username, $icon, $price);

    addLog($_SESSION['user_id'], "Succesvolle het product $username gemaakt");

    header('Location: /org/admin/products/succes/');
    exit();
}

header('Location: /org/admin/products/');
exit();