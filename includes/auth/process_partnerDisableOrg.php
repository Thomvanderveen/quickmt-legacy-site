<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}


if(isset($_POST['org'])){
    if(!orgExistsByID($_POST['org'])){
        header('Location: /admin/org/');
        exit();
    }

    if(!isAdmin($_SESSION['user_id'])){
        header('Location: /admin/org/');
        exit();
    }

    if(!isPartner($_POST['org'])){
        header('Location: /admin/org/');
        exit();
    }

    $org = getOrganisation($_POST['org']);

    setPartner($org['id'], 0);

    addLog($_SESSION['user_id'], "Succesvolle de organisatie ". $org["name"] . " (".$org['location'].") aangepast naar 'Intern Bedrijf Modus'");

    header('Location: /admin/org/'.$_POST['org'].'/');
    exit();
}

header('Location: /admin/org/');
exit();