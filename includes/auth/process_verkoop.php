<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';
include_once '../php/permHandler.php';
include_once '../php/verkoopHandler.php';
include_once '../php/voorraadHandler.php';
include_once '../php/meldingHandler.php';
include_once '../php/productHandler.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['customer'],$_POST['payment_type'])){
    $klant = $_POST['customer'];
    $klant = strip_tags($klant);
    $klant = strtolower($klant);
    $prijs = $_POST['price'];
    $payment = $_POST['payment_type'];

    if($klant == ""){
        header('Location: /org/verkoop/empty/');
        exit();
    }

    if($payment == ""){
        header('Location: /org/verkoop/empty/');
        exit();
    }

    if($payment != "pin" &&  $payment != "contant"){
        header('Location: /org/verkoop/invalid_payment/');
        exit();
    }

    unset($_POST['customer']);
    unset($_POST['price']);
    unset($_POST['payment_type']);

    if(empty($_POST)){
        header('Location: /org/verkoop/empty_items/');
        exit();
    }

    foreach ($_POST as $key => $post){
        if(empty($post) || $post == ""){
            unset($_POST[$key]);
        }
    }

    foreach ($_POST as $key => $post){
        if(!is_numeric($key) || !is_numeric($post)){
            unset($_POST[$key]);
        }
    }

    if(empty($_POST)){
        header('Location: /org/verkoop/empty_items/');
        exit();
    }

    if(!hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.verkoop")){
        header('Location: /home/');
        exit();
    }

    if(!hasAccess($_SESSION['org'], $_SESSION['user_id'])){
        header('Location: /');
        exit();
    }
    
    $org = getOrganisation($_SESSION['org']);

    foreach ($_POST as $key => $post){
        updateVoorraadAfterVerkoop($_SESSION['org'], $key, $post);
        $nieuweVoorraad = getVoorraad($_SESSION['org'], $key);
        $product = getProductByID($key);
        if($nieuweVoorraad < 10){
            $meldingGroupID = 'VoorraadBijnaOp::' . rand(111111111,999999999);
            foreach (getAllUsersFromOrg($_SESSION['org']) as $user){
                if(isAdmin($user['id'])) continue;
                if($user['active'] == 0) continue;
                if(hasPerms($_SESSION['org'], $user['id'], 'page.overzicht.voorraad.manage')){
                    createMelding($user['id'],0,'high', 'fa-box-open', 'Het volgende product: ' . $product['name'] . ' is (bijna) op bij het bedrijf: ' . $org['name'], $meldingGroupID);
                }
            }
        }
    }

    $prijs = round($prijs, 2);

    addVerkoop($_SESSION['user_id'], $_SESSION['org'], $klant, $payment, $_POST, $prijs);

    $user = getUserInfo($_SESSION['user_id']);

    addLog($_SESSION['user_id'], "Succesvolle een verkoop toegevoegd voor de gebruiker ".$user['username']." - (".$user['name'].") in de organisatie ". $org['name']. " (".$org['location'].")");

    header('Location: /org/verkoop/success/');
    exit();
}
