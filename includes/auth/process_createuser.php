<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

ob_start();
include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['username'], $_POST['name'])){
    $username = ltrim(rtrim(strip_tags($_POST['username'])));
    $name = ltrim(rtrim(strip_tags($_POST['name'])));

    if($username == ""){
        header('Location: /admin/users/name_empty/');
        exit();
    }

    if($name == ""){
        header('Location: /admin/users/name_empty/');
        exit();
    }

    if(userExistsByUsername($username)){
        header('Location: /admin/users/name_duplicate/');
        exit();
    }

    if(!isAdmin($_SESSION['user_id'])){
        header('Location: /admin/users/user_error/');
        exit();
    }

    $password = generateRandomString();

    createUser($username, $name, $password);

    $_SESSION['TMP_username'] = $username;
    $_SESSION['TMP_pass'] = $password;
    $_SESSION['TMP_code'] = getUserInfoFromName($username)['code'];

    addLog($_SESSION['user_id'], "Succesvolle de gebruiker $username - ($name) gemaakt");

    header('Location: /admin/users/users_succes/');
    exit();
}

header('Location: /admin/org/user_error/');
exit();


function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}