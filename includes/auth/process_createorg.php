<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

include '../../errorHandler.php';
register_shutdown_function('shutdownErrorFunction', $_SESSION);

include_once 'db_connect.php';
include_once 'functions.php';
include_once '../php/logHandler.php';
include_once '../php/orgHandler.php';
include_once '../php/userInfo.php';

sec_session_start();

if(login_check($mysqli) != true) {
    header('Location: /error/');
    exit();
}

if(isset($_POST['name'], $_POST['location'], $_POST['partner'])){
    $name = ltrim(rtrim(strip_tags($_POST['name'])));
    $location = ltrim(rtrim(strip_tags($_POST['location'])));
    $partner = $_POST['partner'];

    if(orgExistsByName($name)){
        header('Location: /admin/org/org_duplicate/');
        exit();
    }

    if(!isAdmin($_SESSION['user_id'])){
        header('Location: /admin/org/org_error/');
        exit();
    }

    if(!($partner == 0 || $partner == 1)){
        header('Location: /admin/org/org_error/');
        exit();
    }

    createOrganisation($name, $location, $partner);

    addLog($_SESSION['user_id'], "Succesvolle de organisatie $name - ($location) gemaakt");

    header('Location: /admin/org/org_succes/');
    exit();
}

header('Location: /admin/org/org_error/');
exit();