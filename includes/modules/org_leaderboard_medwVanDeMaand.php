<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $medwVanDeMaand = getAllMedwVanDeMaandInfo();  ?>
<?php $place = 1; ?>
<div class="card shadow mb-4">
    <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">Medewerker Van De Maand</h6>
    </div>
    <div class="card-body">
        <div class="table">
            <table class="table table-bordered" id="adminOrgTable">
                <thead>
                <tr>
                    <th style="width: 20%">#</th>
                    <th style="width: 40%">Naam</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($medwVanDeMaand as $user){ ?>
                    <?php if(!isActiveUser($user['id'])) continue; ?>
                    <?php if(isAdmin($user['id'])) continue; ?>
                    <tr>
                        <td ><?php print $place ?></td>
                        <td ><?php print $user['name']; ?></td>
                        <?php $place++; ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
