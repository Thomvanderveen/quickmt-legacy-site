<?php $afwezigheden = getAlleAfwezigheden($_SESSION['org']);?>

<div class="card shadow mb-4">
    <a href="#afwezigheid_open" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="afwezigheid_open">
        <h6 class="m-0 font-weight-bold text-primary">Open Afwezigheidsmeldingen</h6>
    </a>
    <div class="collapse hide" id="afwezigheid_open">
        <div class="card-body">
            <?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

foreach ($afwezigheden as $afwezigheid){
                if($afwezigheid['status'] != 'in behandeling') continue;
                printAfwezigheid($afwezigheid, true);
            }
            ?>
        </div>
    </div>
</div>

<div class="card shadow mb-4">
    <a href="#afwezigheid_archive" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="afwezigheid_archive">
        <h6 class="m-0 font-weight-bold text-primary">Archief</h6>
    </a>
    <div class="collapse hide" id="afwezigheid_archive">
        <div class="card-body">
            <?php
            foreach ($afwezigheden as $afwezigheid){
                if($afwezigheid['status'] == 'in behandeling') continue;
                printAfwezigheid($afwezigheid, false);
            }
            ?>
        </div>
    </div>
</div>
