<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Product Selector</h1>
    <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.products.manage")){ ?>
        <?php if(!(isset($request[3]) && is_numeric($request[3]) && orgHasProduct($_SESSION['org'], $request[3]))){ ?>
            <a href="" data-toggle="modal" data-target="#orgCreateProduct" class="btn btn-md btn-primary shadow-sm">Product Toevoegen</a>
        <?php } ?>
    <?php } ?>
</div>
<p class="font-weight-bold">Op deze pagina kunt u producten toevoegen en aanpassen. <br><span class="text-danger">Er kan data verloren gaan bij het uitschakelen van producten</span></p>

<?php if(isset($request[3]) && $request[3] == "succes"){ ?>
    <div class="card bg-success text-white shadow">
        <div class="card-body">
            U heeft succesvol een product toegevoegd
        </div>
    </div>
    <br>
<?php }?>
<?php if(isset($request[3]) && $request[3] == "succes_deleted"){ ?>
    <div class="card bg-success text-white shadow">
        <div class="card-body">
            U heeft succesvol een product verwijderd
        </div>
    </div>
    <br>
<?php }?>
<?php if(isset($request[3]) && $request[3] == "error"){ ?>
    <div class="card bg-success text-white shadow">
        <div class="card-body">
            Er is iets fout gegaan bij het aanmaken/bewerken van een product
        </div>
    </div>
    <br>
<?php }?>
