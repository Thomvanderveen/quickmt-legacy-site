<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Gebruikers</h1>
    <?php if((!(isset($request[3]) && is_numeric($request[3]) && userExistsByUserID($request[3]) && !isPendingDeleted($request[3], $_SESSION['org']) && hasAccess($_SESSION['org'], $request[3]))) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users.add")){ ?>
        <a href="" data-toggle="modal" data-target="#addUserToOrg" class="btn btn-md btn-primary shadow-sm">Gebruiker Toevoegen</a>
    <?php }elseif(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users.remove") && $_SESSION['user_id'] != $request[3] && !isAdmin($request[3])){ ?>
        <form method="POST" action="/includes/auth/process_deleteuserfromorg.php" class="user">
            <div class="form-group">
                <button type="submit" class="btn btn-md btn-danger shadow-sm" name="usertodelete" id="usertodelete" value="<?php print $request[3]; ?>">Gebruiker Verwijderen</button>
            </div>
        </form>
    <?php } ?>
</div>
<p class="font-weight-bold">Op deze pagina kunt u gebruikers toevoegen aan de organisatie en de permissies aanpassen per gebruiker. <br><span class="text-danger">De wijzigingen worden direct doorgevoerd</span></p>
