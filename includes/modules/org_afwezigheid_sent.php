<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Afwezigheid Melden</h6>
    </div>
    <div class="card-body">
        <?php if(isset($_SESSION['afwezigheid_verstuurd']) && $_SESSION['afwezigheid_verstuurd'] == 'true'){ unset($_SESSION['afwezigheid_verstuurd']); unset($_POST); ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    De afwezigheid is zojuist verstuurd. Wanneer deze is verwerkt krijg je een melding
                </div>
            </div>
            <br>
        <?php }elseif(isset($_SESSION['afwezigheid_verstuurd']) && $_SESSION['afwezigheid_verstuurd'] == 'false'){ unset($_SESSION['afwezigheid_verstuurd']); unset($_POST); ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De afwezigheid kon niet worden verstuurd
                </div>
            </div>
            <br>
        <?php }?>
        <div class="card text-white shadow">
            <div class="card-body">
                <form method="POST" action="" class="user">
                    <div class="form-group">
                        <label class="text-dark" >Begin</label>
                        <input autocomplete="off" class="form-control" id="date" required name="begin" placeholder="dd-mm-yyyy" type="text"/> <br>
                    </div>
                    <div class="form-group">
                        <label class="text-dark" >Eind</label>
                        <input autocomplete="off" class="form-control" id="date" required name="end" placeholder="dd-mm-yyyy" type="text"/> <br>
                    </div>
                    <div class="form-group">
                        <label class="text-dark" >Reden</label>
                        <input  autocomplete="off" type="text" class="form-control" name="reden" id="omschrijving" placeholder="Omschrijving" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-user btn-block">Aanvragen</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        var date_input=$('input[id="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        var options={
            format: 'dd-mm-yyyy',
            startDate: 'today',
            endDate: '+1m',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>