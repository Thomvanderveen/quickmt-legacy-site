<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $userInfo = getUserInfo($_SESSION['user_id']); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Persoonlijke Code</h6>
    </div>
    <div class="card-body">
        <p>De persoonlijke code is een extra stukje beveiliging welke vereist is om in te kunnen loggen in je account. De code kan via onderstaande knop worden gereset. Deze optie is 1 keer per maand beschikbaar.</p>
        <?php if(canResetCode($userInfo['id']) && !empty($userInfo)){ ?>
            <br>
            <form method="POST" action="/includes/auth/process_codereset.php" name="codereset_form">
                <input type="button"  value="Reset Code" class="btn btn-primary btn-user btn-block" onclick="this.form.submit()">
            </form>
        <?php } elseif (empty($userInfo)){ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Uw profiel kan momenteel niet worden geladen
                </div>
            </div>
        <?php } else { ?>
            <hr>
            <p><span class="font-weight-bold">U kunt uw code weer resetten op: <?php print date("d-m-Y H:i", strtotime("+ 1 month", getLastCodeReset($userInfo['id']))) ?></span></p>
            <?php if(isset($request[1])&&$request[1]  == "code_reset"){ ?>
                <div class="card bg-success text-white shadow">
                    <div class="card-body">
                        Uw code is gereset! De nieuwe code is: <span class="font-weight-bold"><?php print $userInfo['code']; ?></span>
                    </div>
                </div>
            <?php } ?>
            <?php if(isset($request[1])&&$request[1]  == "code_invalid"){ ?>
                <div class="card bg-danger text-white shadow">
                    <div class="card-body">
                        Het resetten van uw persoonlijke code is momenteel niet mogelijk!
                    </div>
                </div>
            <?php } ?>

        <?php } ?>
    </div>
</div>