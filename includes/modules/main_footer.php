<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><script src="/includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="/includes/vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="/includes/js/quickmt.js"></script>

<script src="/includes/js/forms.js"></script>
<script src="/includes/js/sha512.js"></script>

<script src="/includes/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/includes/vendor/chart.js/Chart.min.js"></script>
<script src="/includes/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script src="/includes/js/tables.js"></script>
<script src="/includes/js/charts.js"></script>
</body>

</html>
