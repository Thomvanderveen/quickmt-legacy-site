<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notitie toevoegen?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="font-weight-bold"><span class="text-danger">De notities zijn voor iedereen binnen de organisatie zichtbaar. Denk dus eerst goed na voordat je een notitie plaatst</span></p>
                <br>
                <form method="POST" action="/includes/auth/process_addnote.php" class="user" name="addNote_form" id="addNote_form">
                    <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" name="titel" id="titel" placeholder="Onderwerp" required>
                    </div>
                    <div class="form-group">
                        <textarea autocomplete="off" type="text" class="form-control" name="bericht" id="bericht" placeholder="Bericht" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuleren</button>
                <a class="btn btn-primary text-white" onclick="document.getElementById('addNote_form').submit();">Notitie Toevoegen</a>
            </div>
        </div>
    </div>
</div>