<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $users = getAllUsersFromOrg($_SESSION['org'])?>
<?php foreach ($users as $user) { ?>
    <?php $omzetPIN = getOmzetSinceLastoonRest($_SESSION['org'],$user['id'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'pin'); ?>
    <?php $omzetCONTANT = getOmzetSinceLastoonRest($_SESSION['org'],$user['id'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'contant'); ?>
    <?php $totaleOmzet = $omzetPIN + $omzetCONTANT; ?>
    <?php $persoonlijkeDoel = getPersoonlijkDoel($_SESSION['org'], $user['id'])['challenge']; ?>
    <?php if(isAdmin($user['id'])) continue; ?>
    <?php if(getAllUserOmzetAltijd($_SESSION['org'],$user['id'], 'pin') >= 1 || getAllUserOmzetAltijd($_SESSION['org'],$user['id'], 'contant') >= 1){ ?>
        <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#omzet_<?php print $user['name'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="omzet_<?php print $user['name'] ?>">
            <h6 class="m-0 font-weight-bold text-primary">
                <?php print $user['name'] ?>
                | Doel  
                
                <?php if($persoonlijkeDoel > $totaleOmzet){ ?>
                   <i class="fas fa-times text-danger"></i>
                <?php }else{ ?>
                    <i class="fas fa-check text-success"></i>
                <?php } ?>
                 Planning 
                
                <?php if(getAantalPlanningenAfgelopenWeek($user['id']) >= 16){ ?>
                   <i class="fas fa-check text-success"></i>
                <?php }else{ ?>
                    <i class="fas fa-times text-danger"></i>
                <?php } ?>
            </h6>

        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse hide" id="omzet_<?php print $user['name'] ?>">
            <div class="card-body">


                <div class="row">
                    <?php
                    $statsAantal = getOmzetSinceLastoonRest($_SESSION['org'],$user['id'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'pin');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Laatste Loonberekening</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $statsAantal = getAllUserOmzet(60*24*7,$_SESSION['org'],$user['id'], 'pin');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Week</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $statsAantal = getAllUserOmzet(60*24*30,$_SESSION['org'],$user['id'], 'pin');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Maand</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $statsAantal = getAllUserOmzetAltijd($_SESSION['org'],$user['id'], 'pin');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Totaal</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <?php
                    $statsAantal = getOmzetSinceLastoonRest($_SESSION['org'],$user['id'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'contant');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Laatste Loonberekening</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $statsAantal = getAllUserOmzet(60*24*7,$_SESSION['org'],$user['id'], 'contant');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Week</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $statsAantal = getAllUserOmzet(60*24*30,$_SESSION['org'],$user['id'], 'contant');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Maand</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $statsAantal = getAllUserOmzetAltijd($_SESSION['org'],$user['id'], 'contant');
                    if($statsAantal <= 0){
                        $inlogBorder = "border-left-warning";
                        $inlogText = "text-warning";
                        $inlogIcon = "text-warning";
                    }else{
                        $inlogBorder = "border-left-success";
                        $inlogText = "text-success";
                        $inlogIcon = "text-gray-300";
                    }
                    ?>
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Totaal</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                                    </div>
                                    <div class="col-auto">
                                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    <?php } ?>
<?php } ?>
