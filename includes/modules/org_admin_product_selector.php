<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $products = getAllProducts($_SESSION['org']);?>
<div class="card bg-warning text-white shadow">
    <div class="card-body">
        Let op! Wanneer je een product verwijderd is dit defintief en wordt de voorraad van dit product ook gewist!
    </div>
</div>
<br>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Producten</h6>
    </div>
    <div class="card-body">
        <?php if(!empty($products) >= 1){ ?>
            <div class="table">
                <table class="table table-bordered" id="adminOrgTable">
                    <thead>
                    <tr>
                        <th style="width: 10%">ID</th>
                        <th style="width: 30%">Naam</th>
                        <th style="width: 40%">Prijs</th>
                        <th style="width: 20%">Gelimiteerd</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product){ ?>
                        <tr>
                            <td><?php print $product['id']; ?></td>
                            <td><?php print $product['name']; ?></td>
                            <td><?php print $product['price']; ?></td>
                            <td><?php if(productIsDailyLimited($product['id'])){print "Ja (".getProductLimit($product['id']).")";}else{print "Nee";} ?></td>
                            <td>
                                <?php if(!hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.products.manage")){ ?>
                                    <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php }elseif(orgHasProduct($_SESSION['org'], $product['id'])){ ?>
                                    <a href="/org/admin/products/<?php print $product['id']; ?>/" class="btn btn-sm btn-primary shadow-sm"><i class="fas fa-edit fa-lg text-white"></i></a>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if(hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.products.manage") && orgHasProduct($_SESSION['org'], $product['id'])){ ?>
                                    <a href="/org/admin/products/<?php print $product['id']; ?>/delete/" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De producten kunnen momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>
