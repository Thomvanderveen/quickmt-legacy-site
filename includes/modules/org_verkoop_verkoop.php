<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $producten = getAllProducts($_SESSION['org'])?>
<?php print "
<script>
    $( document ).ready(function() {
        $.getJSON('/includes/php/verkoopJSON.php', function(result){
            $.each(result, function (key, val) {
                localStorage.setItem('product_'+val['id'], val['price']);
            });
        });
    });
    
    function onInputUpdate() {
        totalPrice();
    }
    
    function onUserNameInput(username, org) {
        $('#limited_product_list').empty();
        $.getJSON('/api/getItemsSoldToday.php?org='+org+'&user='+username, function(result){
            if (result.length == 0 ) {
                $('#limited_product_popup').hide();
            }else{
                $('#limited_product_popup').show();
                $.each(result, function (key, val) {
                    handleUsernameUpdate(val[1],val[0]);
                });
            }
        });
              
    }
    
    function handleUsernameUpdate(product,aantal){
       $('#limited_product_list').append('<li><span class=\"font-weight-bold\">'+product+': </span>'+aantal+' keer</li>');
    }
    
    function totalPrice(){
        var total = 0;
        $('input[id=\"productInput\"').each(function() {
            var aantal = $(this).val();  
            var product = $(this).attr(\"name\");
            var price = localStorage.getItem('product_'+product);
            total += (aantal * price);
        });
        total = Math.round((total + Number.EPSILON) * 100) / 100;
        total = total.toFixed(2);
        document.getElementById('totalPrice').value = total;
    }
</script>
"?>
<?php if(isset($request[2]) && $request[2] == "empty") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Niet alle velden zijn juist ingevuld
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "invalid_payment") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Ongeldige Betalingsoptie
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "teveel_items") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Stop de verkoop! Deze persoon heeft (als je deze verkoop erbij optelt) teveel vitamines gekocht!
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "empty_items") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Er waren geen items ingevuld
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "success") { ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De verkoop is succesvol voltooid
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<br>
<br>
<div class="row" id="limited_product_popup" style="display: none">
    <div class="col-lg-6">
        <div class="card bg-warning text-white shadow mb-3">
            <div class="card-body">
                <span class="font-weight-bold">Let op!</span><br>
                Deze klant mag de volgende aantallen van de gelimiteerde producten vandaag nog kopen.<br>
                <ul id="limited_product_list">
                    <li></li>
                </ul>
            </div>
            </div>
        </div>
</div>
<hr>
<form method="POST" action="/includes/auth/process_verkoop.php" class="user" name="verkoop_form">
    <div class="form-group">
        <input autocomplete="off" type="text" class="form-control form-control-user" onchange="onUserNameInput(this.value, <?php print $_SESSION['org']; ?>);" name="customer" id="customer" placeholder="Klant">
    </div>
    <hr>
    <div class="row">
        <?php foreach ($producten as $key => $product){ ?>
        <div class="col-xl-6 col-lg-6">
            <div class="row">
                <div class="col-sm-3 ml-4 text-right">
                    <p class="h4 col-sm-2 text-right"><?php print ucfirst($product['name']);?></p>
                </div>
                <div class="col-sm-8 ml-4 text-right">
                    <input autocomplete="off"  type="text" onchange="onInputUpdate();" name="<?php print $product['id']?>" product-id="<?php print $product['id']?>" id="productInput" class="form-control form-control-user bg-white">
                </div>
            </div>
        </div>
            <br>
            <br>
            <br>
        <?php } ?>
    </div>
    <hr>
    <p class="h3">Totaal: €<input type="number" name="price" id="totalPrice" class="bg-white" value="0" readonly></p>
    <br>
    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <button type="submit" class="btn btn-primary btn-user btn-block" name="payment_type" id="payment_type" value="pin">Pin</button>
        </div>
        <br>
        <br>
        <br>
        <div class="col-xl-6 col-lg-6">
            <button type="submit" class="btn btn-primary btn-user btn-block" name="payment_type" id="payment_type" value="contant">Contant</button>
        </div>
    </div>
</form>