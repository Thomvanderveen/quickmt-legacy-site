<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $pages = getAllPages();?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Pagina's</h6>
    </div>
    <div class="card-body">
        <?php if(!empty($pages) >= 1){ ?>
            <div class="table">
                <table class="table table-bordered" id="adminOrgTable">
                    <thead>
                    <tr>
                        <th style="width: 10%">ID</th>
                        <th style="width: 30%">Naam</th>
                        <th style="width: 40%">Beschrijving</th>
                        <th style="width: 20%">Geactiveerd</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($pages as $page){ ?>
                        <?php if(isPartner($_SESSION['org']) && $page['partner'] == 0) continue; ?>
                        <?php if(!isPartner($_SESSION['org']) && $page['intern'] == 0) continue; ?>
                        <tr>
                            <td><?php print $page['id']; ?></td>
                            <td><?php print $page['name']; ?></td>
                            <td><?php print $page['description']; ?></td>
                            <td><?php if($page['active'] == 0){print "Niet meer beschikbaar"; }elseif(orgHasPage($_SESSION['org'], $page['id'])){print "Ja";}else{print "Nee";} ?></td>
                            <td>
                                <?php if(!hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.pages.manage")){ ?>
                                    <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php }elseif($page['active'] == 0){ ?>
                                    <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php }elseif(orgHasPage($_SESSION['org'], $page['id'])){ ?>
                                    <a href="/org/admin/pages/<?php print $page['id']; ?>/disable/" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } else { ?>
                                    <a href="/org/admin/pages/<?php print $page['id']; ?>/enable/" class="btn btn-sm btn-success shadow-sm"><i class="fas fa-check fa-lg text-white"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De pagina's kunnen momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>