<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $notes = getNotities($_SESSION['org'])?>
<?php $temp = false; ?>
<?php if(isset($request[2]) && $request[2] == "empty") { $temp = true; ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Niet alle velden zijn juist ingevuld
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "success") { $temp = true;  ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De notitie is succesvol toegevoegd
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($request[2]) && $request[2] == "no_perms") { $temp = true;  ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    Je hebt niet de juiste permissies
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($_SESSION['notitie_deleted']) && $_SESSION['notitie_deleted'] == "true") { unset($_SESSION['notitie_deleted']);  $temp = true;  ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-success text-white shadow mb-3">
                <div class="card-body">
                    De geselecteerde notitie is succesvol verwijderd
                </div>
            </div>
        </div>
    </div>
<?php }elseif(isset($_SESSION['notitie_deleted']) && $_SESSION['notitie_deleted'] == "false") { unset($_SESSION['notitie_deleted']);  $temp = true;  ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-danger text-white shadow mb-3">
                <div class="card-body">
                    De notitie kon niet worden verwijderd
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if(!empty($notes)){ ?>
    <?php foreach ($notes as $note) { ?>
        <div class="card shadow mb-4">
            <a href="#note_calc_<?php print $note['id'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="#note_calc_<?php print $note['id'] ?>">
                <h6 class="m-0 font-weight-bold text-primary"><?php print $note['titel'] ?> - <span><?php print getUserInfo($note['user_id'])['username']; ?> (<?php print getUserInfo($note['user_id'])['name']; ?>)</span> - <span><?php print date("d-m-Y", $note['createdOn']); ?></span> <?php if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.notities.remove")){ ?><span class="text-danger font-weight-bold" onclick="window.location.href='/org/notities/<?php print $note['id'] ?>/delete/'">Notitie Verwijderen</span> <?php } ?></h6>
            </a>
            <div class="collapse hide" id="note_calc_<?php print $note['id'] ?>">
                <div class="card-body">
                    <p><?php print $note['text'] ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
<?php }elseif($temp == false){ ?>
    <div class="row">
        <div class="col-lg-6">
            <div class="card bg-warning text-white shadow mb-3">
                <div class="card-body">
                    Er zijn momenteel nog geen notities
                </div>
            </div>
        </div>
    </div>
<?php } ?>