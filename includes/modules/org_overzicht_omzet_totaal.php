<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><hr>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h5 class="h5 mb-0 text-gray-800">Totale Omzet</h5>
</div>
<div class="row">
    <?php
    $statsAantal = getOmzetTotalSinceLastoonRest($_SESSION['org'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'pin');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Laatste Loonberekening</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $statsAantal = getTotaleOmzet(60*24*7,$_SESSION['org'], 'pin');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Week</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $statsAantal = getTotaleOmzet(60*24*30, $_SESSION['org'], 'pin');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Maand</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $statsAantal = getTotaleOmzetAltijd($_SESSION['org'], 'pin');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Totaal</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-credit-card fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <?php
    $statsAantal = getOmzetTotalSinceLastoonRest($_SESSION['org'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'contant');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Laatste Loonberekening</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $statsAantal = getTotaleOmzet(60*24*7,$_SESSION['org'], 'contant');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Week</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $statsAantal = getTotaleOmzet(60*24*30, $_SESSION['org'], 'contant');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Maand</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $statsAantal = getTotaleOmzetAltijd($_SESSION['org'], 'contant');
    if($statsAantal <= 0){
        $inlogBorder = "border-left-warning";
        $inlogText = "text-warning";
        $inlogIcon = "text-warning";
    }else{
        $inlogBorder = "border-left-success";
        $inlogText = "text-success";
        $inlogIcon = "text-gray-300";
    }
    ?>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card <?php print $inlogBorder; ?> shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold <?php print $inlogText; ?> text-uppercase mb-1">Totaal</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php print $statsAantal; ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-money-bill-wave fa-3x <?php print $inlogIcon; ?>"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>