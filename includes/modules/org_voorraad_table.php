<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $products = getAllProducts($_SESSION['org']);?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Voorraad</h6>
    </div>
    <div class="card-body">
        <?php if(!empty($products) >= 1){ ?>
            <div class="table">
                <table class="table table-bordered" id="adminOrgTable">
                    <thead>
                    <tr>
                        <th style="width: 10%">ID</th>
                        <th style="width: 50%">Naam</th>
                        <th style="width: 40%">Voorraad</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($products as $product){ ?>
                        <?php if(!orgHasProduct($_SESSION['org'], $product['id'])) continue; ?>
                        <tr>
                            <td><?php print $product['id']; ?></td>
                            <td><?php print $product['name']; ?></td>
                            <td><?php print getVoorraad($_SESSION['org'], $product['id']); ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De producten kunnen momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>