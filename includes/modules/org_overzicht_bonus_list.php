<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $bonussen = getBonussen($_SESSION['org']);?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Bonussen</h6>
    </div>
    <div class="card-body">
        <?php if(isset($_SESSION['bonus_removed']) && $_SESSION['bonus_removed'] == 'true'){ unset($_SESSION['bonus_removed']); unset($_POST); ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    De bonus is succesvol verwijderd
                </div>
            </div>
            <br>
        <?php }?>
        <?php if(isset($_SESSION['bonus_removed']) && $_SESSION['bonus_removed'] == 'false'){ unset($_SESSION['bonus_removed']); unset($_POST); ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De bonus kon niet worden verwijderd
                </div>
            </div>
            <br>
        <?php }?>
        <?php if(!empty($bonussen)){ ?>
            <div class="table">
                <table class="table table-bordered" id="uitgavenTable">
                    <thead>
                    <tr>
                        <th style="width: 20%">Datum</th>
                        <th style="width: 20%">Gebruiker</th>
                        <th style="width: 40%">Omschrijving</th>
                        <th style="width: 20%">Bedrag</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($bonussen as $bonus){ ?>
                        <?php $user = getUserInfo($bonus['user_id'])?>
                        <tr>
                            <td><?php print date("d-m-Y", $bonus['time']); ?></td>
                            <td><?php print $user['name']; ?> (<?php print $user['username']; ?>)</td>
                            <td><?php print $bonus['omschrijving']; ?></td>
                            <td>€<?php print $bonus['total']; ?></td>
                            <td>
                                <?php if($bonus['time'] > getOrganisation($_SESSION['org'])['lastLoonReset'] && hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.bonus.remove")){ ?>
                                    <a href="/org/overzicht/bonus/delete/<?php print $bonus['id']; ?>/" class="btn btn-sm btn-danger shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } else { ?>
                                    <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De bonussen kunnen momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>