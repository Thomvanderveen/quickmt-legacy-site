<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><body class="bg-gradient-success">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">QuickMT Sollicitatie Formulier</h1>
                                </div>
                                <?php if(isset($request[1]) && $request[1] == "-"){ ?>
                                    <div class="card bg-primary text-white shadow">
                                        <div class="card-body">
                                            Jouw sollicitatie is succesvol verzonden! De status van behandeling vind je in onze discordserver: <a class="text-white" href="https://discord.gg/GDhhy5A">https://discord.gg/GDhhy5A</a>
                                        </div>
                                    </div>
                                    <br>
                                <?php } ?>
                                <form method="POST" action="/includes/auth/process_sollicitatie.php" class="user" name="login_form">
                                    <div class="form-group">
                                        <label>Voor welke functie solliciteert u?</label> <span class="text-danger">*</span>
                                        <select required class="form-control" name="functie">
                                            <option value="medw">Medewerker</option>
                                            <option value="dev">Developer</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Indien u solliciteert als verkoop medewerker, aan welk bedrijf geeft u een voorkeur?</label>
                                        <select class="form-control" name="comp">
                                            <option value="0" selected="selected">Geen voorkeur</option>
                                            <?php foreach (getAllOrganisations() as $org){ ?>
                                                <?php if($org['id'] == 1) continue; ?>
                                                <?php if(!isActiveOrg($org['id'])) continue; ?>
                                                <?php if($org['fantasy'] == 1) continue; ?>
                                                <?php if (isPartner($org['id'])) continue; ?>
                                                <option value="<?php print $org['id'] ?>"><?php print $org['name'] ?> (<?php print $org['location'] ?>)</option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw in game naam?</label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="username" class="form-control" placeholder="Captain_DDG"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw echte naam?</label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="name" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Heeft u de tutorial gehaald?</label> <span class="text-danger">*</span>
                                        <select required class="form-control" name="tutorial">
                                            <option value="0">Nee</option>
                                            <option value="1">Ja</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw online time? </label> <span class="text-danger">*</span>
                                        <input  autocomplete="off" type="text" required name="online_time" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Sinds wanneer speelde u Minetopia? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="date" required name="start_date" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Hoeveel uur per dag bent u online? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="online_time_day" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Hoeveel van deze uren kunt u in de winkel stoppen? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="online_time_day_shop" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw level? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="level" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>In welke stad staat u ingeschreven? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="city" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw leeftijd? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="leeftijd" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Heeft u Teamspeak/Discord?</label> <span class="text-danger">*</span>
                                        <select required class="form-control" name="contact">
                                            <option value="nee">Nee</option>
                                            <option value="ja">Ja</option>
                                            <option value="no_voice">Ja, maar ik kan er niet op praten</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Heeft u op dit moment een andere baan?</label> <span class="text-danger">*</span>
                                        <select required class="form-control" name="job">
                                            <option value="ja">Ja</option>
                                            <option value="nee">Nee</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Zoja, wat is uw baan? </label>
                                        <input autocomplete="off" type="text" name="job_text" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw werkervaring binnen MineTopia? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="werkervaring" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw VOG status?</label> <span class="text-danger">*</span>
                                        <select required class="form-control" name="vog">
                                            <option value="nee">Negatief</option>
                                            <option value="ja">Positief</option>
                                            <option value="idk">Weet ik niet</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat is uw motivatie om bij ons te werken? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="motivatie" class="form-control" placeholder=""/>
                                    </div>
                                    <div class="form-group">
                                        <label>Wat zijn uw contact gegevens? </label> <span class="text-danger">*</span>
                                        <input autocomplete="off" type="text" required name="contact_info" class="form-control" placeholder=""/>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-user btn-block" value="Verzenden">Verzenden</button>
                                </form>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


