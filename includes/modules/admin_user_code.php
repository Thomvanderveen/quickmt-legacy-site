<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php $user = getUserInfo($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?php print getLanguages("admin_user_code", "user_personal_code"); ?></h6>
    </div>
    <div class="card-body">
        <?php if (empty($user)){ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_code", "user_personal_code_cannot_load"); ?>
                </div>
            </div>
        <?php } elseif(!isActiveUser($user['id'])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_code", "user_personal_code_deleted"); ?>
                </div>
            </div>
        <?php } elseif($user['id'] == $_SESSION['user_id']) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_code", "user_personal_code_own"); ?>
                </div>
            </div>
        <?php } elseif(isAdmin($user['id'])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_code", "user_personal_code_admin"); ?>
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "code_error") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_code", "user_personal_code_error"); ?>
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "code_success") { ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_user_code", "user_personal_code_reset"); ?>
                    <span class="font-weight-bold"><?php print $user['code']; ?></span>
                </div>
            </div>
        <?php } else { ?>
            <form method="POST" action="/includes/auth/process_admincodereset.php" name="adminCodeReset_form">
                <button type="submit" class="btn btn-primary btn-user btn-block" name="codereset" value="<?php print $user['id']; ?>"><?php print getLanguages("admin_user_code", "user_personal_code_submit"); ?></button>
            </form>
        <?php }?>
    </div>
</div>