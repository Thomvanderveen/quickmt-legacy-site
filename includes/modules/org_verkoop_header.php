<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

$omzetPIN = getOmzetSinceLastoonRest($_SESSION['org'],$_SESSION['user_id'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'pin');
$omzetCONTANT = getOmzetSinceLastoonRest($_SESSION['org'],$_SESSION['user_id'],getOrganisation($_SESSION['org'])['lastLoonReset'], 'contant');
$totaleOmzet = $omzetPIN + $omzetCONTANT;
$persoonlijkeDoel = getPersoonlijkDoel($_SESSION['org'], $_SESSION['user_id'])['challenge'];
$userBonus = getBonussen_Bedrag($_SESSION['org'], $_SESSION['user_id'], getOrganisation($_SESSION['org'])['lastLoonReset']);
?>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Verkoop Pagina</h1>
</div>
<p>
    <span class="h5">Huidige Loon: €<?php print round(getOmzetTotalPerUserSinceLastoonRest($_SESSION['org'], $_SESSION['user_id'], getOrganisation($_SESSION['org'])['lastLoonReset']) * getLoonPercentage($_SESSION['org'],$_SESSION['user_id']), 2);?> </span>
    <?php if($userBonus['total'] > 0){ ?>
    <span class="h5"> (+ €<?php print $userBonus['total']; ?> bonus)</span>
    <?php }else if($userBonus['total'] < 0){ ?>
    <span class="h5"> (€<?php print $userBonus['total']; ?> bonus)</span>
    <?php } ?>
</p>
<?php if($persoonlijkeDoel > $totaleOmzet){ ?>
<p>
    <span class="h5 text-danger font-weight-bold">Resterende doel: €<?php print $persoonlijkeDoel - $totaleOmzet?> </span>
</p>
<?php }else{ ?>
    <span class="h5 text-success font-weight-bold">Jij hebt je persoonlijke doel deze periode behaald!</span>
<?php } ?>
<script>
    ifvisible.setIdleDuration(600);
    var userid = '<?php echo $_SESSION["user_id"]; ?>';
    var username = '<?php echo $_SESSION["username"]; ?>';
    var login_string = '<?php echo $_SESSION["login_string"]; ?>';
    var org = '<?php echo $_SESSION["org"]; ?>';
    ifvisible.idle(function(){
        document.body.style.opacity = 0.3;
    });
    ifvisible.blur(function() {
        document.body.style.opacity = 0.3;
    });
    ifvisible.wakeup(function(){
        window.location.reload();
    });
    ifvisible.onEvery(60, function(){
        $.ajax({
            method: "POST",
            url: "/api/changeStatus.php",
            data: { 'userid': userid, 'username': username, 'login_string': login_string, 'org': org }
        });
    });
    $( document ).ready(function() {
        $.ajax({
            method: "POST",
            url: "/api/changeStatus.php",
            data: { 'userid': userid, 'username': username, 'login_string': login_string, 'org': org }
        });
    });
</script>
