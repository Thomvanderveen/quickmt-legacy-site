<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $user = getUserInfo($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Wachtwoord Resetten</h6>
    </div>
    <div class="card-body">
        <?php if (empty($user)){ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De gebruiker kan niet worden geladen
                </div>
            </div>
        <?php } elseif(!isActiveUser($user['id'])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Deze functie is uitgeschakeld bij verwijderde gebruikers
                </div>
            </div>
        <?php } elseif($user['id'] == $_SESSION['user_id']) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Het is niet mogelijk via dit menu je eigen wachtwoord te resetten
                </div>
            </div>
        <?php } elseif(isAdmin($user['id'])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Het is niet mogelijk via dit menu een bewerking uit te voeren bij een admin
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "password_error") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    Het wachtwoord kon niet worden aangepast
                </div>
            </div>
        <?php } elseif(isset($request[3],$_SESSION["TMP_PWDRESET"])&&$request[3]  == "password_succes") { ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    Het wachtwoord is gereset. Het nieuwe wachtwoord is <span class="font-weight-bold"><?php print $_SESSION["TMP_PWDRESET"]; ?></span>
                </div>
            </div>
            <?php unset($_SESSION["TMP_PWDRESET"]); ?>
        <?php } else { ?>
            <form method="POST" action="/includes/auth/process_adminpasswordreset.php" name="adminPassReset_form">
                <button type="submit" class="btn btn-primary btn-user btn-block" name="pwrdrst" value="<?php print $user['id']; ?>">Wachtwoord Resetten</button>
            </form>
        <?php }?>
    </div>
</div>