<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Loonpercentage aanpassen</h6>
    </div>
    <div class="card-body">
            <form method="POST" action="/includes/auth/process_userLoonPercentage.php" name="namechange_form">
                <p><span class="font-weight-bold">Huidige Percentage: </span> <?php print getLoonPercentage($_SESSION['org'], $request[3]) * 100 . "%"; ?></p>
                <div class="form-group">
                    <input autocomplete="off" type="text" class="form-control form-control-user" name="percentage" id="name" value="<?php print getLoonPercentage($_SESSION['org'], $request[3]) * 100; ?>" required>
                </div>
                <button type="submit"  value="<?php print $request[3] . "-".$_SESSION['org']; ?>" name="user_org" class="btn btn-primary btn-user btn-block">Aanpassen</button>
            </form>
    </div>
</div>