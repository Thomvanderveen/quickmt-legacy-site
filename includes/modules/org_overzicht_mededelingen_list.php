<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><h1 class="h3 mb-4 text-gray-800">Door jou verstuurde meldingen</h1>
<?php $meldingen = getVerstuurdeMeldingen($_SESSION['user_id']);?>
<?php foreach ($meldingen as $melding){ ?>

    <div class="card shadow mb-4">
        <a href="#melding_<?php print $melding['id'] ?>" class="d-block card-header py-3 collapsed" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="melding_<?php print $melding['id'] ?>">
            <h6 class="m-0 font-weight-bold text-primary">
                Melding (Prioriteit: <?php print strtoupper($melding['prioriteit']); ?>) - <?php print date("F j, Y", $melding['created_on']); ?><br>
            </h6>
        </a>
        <div class="card-body">
            <?php print $melding['message']; ?>
        </div>
        <div class="collapse hide" id="melding_<?php print $melding['id'] ?>">
            <div class="card-footer">
                <?php $gelezenList = array(); $ongelezenList = array(); ?>
                <?php foreach (getMeldingenMetZelfdeGroupID($melding['melding_group_id']) as $zelfMelding){ ?>
                    <?php if($zelfMelding['gelezen'] == 1){
                        $gelezenList[] = getUserInfo($zelfMelding['user_id'])['name'];
                    }else{
                        $ongelezenList[] = getUserInfo($zelfMelding['user_id'])['name'];
                    } ?>
                <?php } ?>
                <span class="h5 font-weight-bold">Gelezen</span><br>
                <?php foreach ($gelezenList as $users){ ?>
                    <span class="h9"><?php print $users; ?></span><br>
                <?php } ?>
                <br>
                <span class="h5 font-weight-bold">Niet Gelezen</span><br>
                <?php foreach ($ongelezenList as $users){ ?>
                    <span class="h9"><?php print $users; ?></span><br>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>