<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?>

<?php $org = getOrganisation($request[2]); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?php print getLanguages("admin_organisations_location", "org_location_change");?></h6>
    </div>
    <div class="card-body">
        <?php if (empty($org)){ ?>
        <div class="card bg-danger text-white shadow">
            <div class="card-body">
                <?php print getLanguages("admin_organisations_location", "org_location_cannot_load");?>
            </div>
        </div>
        <?php } elseif(isset($request[2])&&$request[2]  == "1") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_location", "org_location_cannot_edit_admin");?>
                </div>
            </div>
        <?php } elseif(!isActiveOrg($request[2])) { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_location", "org_location_cannot_edit_deleted");?>
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "location_changed") { ?>
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_location", "org_location_changed");?>
                    <span class="font-weight-bold"><?php print $org['location']; ?></span>
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "location_error") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_location", "org_location_error");?>
                </div>
            </div>
        <?php } elseif(isset($request[3])&&$request[3]  == "location_empty") { ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    <?php print getLanguages("admin_organisations_location", "org_location_empty");?>
                </div>
            </div>
        <?php } else { ?>
            <form method="POST" action="/includes/auth/process_orglocationchange.php" name="namechange_form">
                <p><span class="font-weight-bold"><?php print getLanguages("admin_organisations_location", "org_location_current");?></span> <?php print $org['location']; ?></p>
                <div class="form-group">
                    <input autocomplete="off" type="text" class="form-control form-control-user" name="name" id="name" value="<?php print $org['location']; ?>" placeholder="<?php print getLanguages("admin_organisations_location", "org_location_input_placeholder");?>" required>
                </div>
                <button type="submit"  value="<?php print $org['id']; ?>" name="org" class="btn btn-primary btn-user btn-block"><?php print getLanguages("admin_organisations_location", "org_location_submit");?></button>
            </form>
        <?php }?>
    </div>
</div>