<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php $users = getAllUsersFromOrg($_SESSION['org'])?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="font-weight-bold text-primary">Persoonlijke Doelen</h6>
    </div>
    <div class="card-body">
        <?php if(!empty($users)){ ?>
            <div class="table">
                <table class="table table-bordered" id="adminOrgTable">
                    <thead>
                    <tr>
                        <th style="width: 10%">ID</th>
                        <th style="width: 25%">Naam</th>
                        <th style="width: 25%">Username</th>
                        <th style="width: 20%">Huidige Doel</th>
                        <th style="width: 20%">Aanpassen</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users as $user){ ?>
                        <?php if(isAdmin($user['id'])) continue; ?>
                        <tr>
                            <td><?php print $user['id']; ?></td>
                            <td><?php print $user['username']; ?></td>
                            <td><?php print $user['name']; ?></td>
                            <td><?php print getPersoonlijkDoel($_SESSION['org'], $user['id'])['challenge']?></td>
                            <td>
                                <?php if(isAdmin($user['id']) || !hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.doel.manage")){ ?>
                                    <a class="btn btn-sm btn-secondary shadow-sm"><i class="fas fa-times fa-lg text-white"></i></a>
                                <?php } else { ?>
                                    <form method="POST" action="" class="user" name="adminRemoveVoorraad" id="adminRemoveVoorraad">
                                        <div class="form-group">
                                            <input autocomplete="off" type="text" class="form-control form-control-user" name="updateDoel_User" id="updateDoel_User" placeholder="Doel" required><br>
                                            <button type="submit" class="btn btn-primary btn-user btn-block" name="updateDoel" value="<?php print $user['id']; ?>">Aanpassen</button>
                                        </div>
                                    </form>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php }else{ ?>
            <div class="card bg-danger text-white shadow">
                <div class="card-body">
                    De doelen kunnen momenteel niet worden geladen
                </div>
            </div>
        <?php } ?>
    </div>
</div>