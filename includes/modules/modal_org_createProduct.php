<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><div class="modal fade" id="orgCreateProduct" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Product Toevoegen</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/includes/auth/process_createProduct.php" class="user" name="orgAddProductID" id="orgAddProductID">
                    <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" name="username" id="username" placeholder="Naam" required>
                    </div>
                    <?php if(!isPartner($_SESSION['org'])){ ?>
                        <div class="form-group">
                            <input autocomplete="off" type="text" class="form-control" name="icon" id="name" placeholder="Icon" required>
                            <span class="text-danger">Het is belangrijk dat hier een bestaand discord emoticon word ingevuld (Voorbeeld :kwiek:)</span>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <input autocomplete="off" type="text" class="form-control" name="price" id="name" placeholder="Prijs" required>
                        <span class="text-danger">Let op! Gebruik om het aantal cent aan te geven GEEN comma maar een punt</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuleren</button>
                <a class="btn btn-primary text-white" onclick="document.getElementById('orgAddProductID').submit();">Aanmaken</a>
            </div>
        </div>
    </div>
</div>