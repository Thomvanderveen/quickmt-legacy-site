<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
?><?php if(isset($request[1]) && $request[1] == "partner_payed" && isPartner($_SESSION['org']) && partnerHasOpenPayment($_SESSION['org'])){
    $payment = getOpenPayment($_SESSION['org']);
    paymentPayed($payment['id'],$_SESSION['user_id']);
}?>

<?php if($_SESSION['org'] ==1){ ?>
    <?php require_once './includes/pages/admin_home.php'; ?>
<?php } else { ?>
    <h1 class="h3 mb-4 text-gray-800">Home</h1>
    <?php if(isPartner($_SESSION['org']) && partnerHasOpenPayment($_SESSION['org'])){ ?>
    <div class="card bg-warning text-white shadow mb-3">
        <div class="card-body">
            <span class="font-weight-bold">Let op!</span><br>
            Er staat nog een betaling van <span class="font-weight-bold">€<?php print getOpenPayment($_SESSION['org'])['price']; ?></span> open aan Quick Company<br>
            <span class="text-danger">Heb je dit bedrag al wel betaald neem dan even contact op met Julian via discord: <span class="font-weight-bold">ItsJulian#8581</span></span><br>
            <?php if (isAdmin($_SESSION['user_id'])){ ?>
            <a href="/home/partner_payed/" class="btn btn-md btn-primary shadow-sm text-white">Betaald</a>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
    <?php if(hasPendingDeletions($_SESSION['org']) && hasPerms($_SESSION['org'], $_SESSION['user_id'], "page.admin.users")) { ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="card bg-warning text-white shadow mb-3">
                    <div class="card-body">
                        De volgende personen zijn aangevraagt om verwijderd te worden en zullen automatisch na de volgende loonberekening worden verwijderd. Deze gebruikers hebben al geen permissies meer op de website en kunnen dus nergens bij!
                        <ul>
                            <?php foreach (getAllUsersFromOrg($_SESSION['org']) as $user) { ?>
                                <?php if(!isPendingDeleted($user['id'], $_SESSION['org'])) continue?>
                                <li><?php print $user['name']; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <p>Welkom op de bedrijfspagina van <span class="font-weight-bold"><?php print getOrganisation($_SESSION['org'])['name']; ?></span>.<br>
        Op de andere pagina´s kunt u aan de slag gaan! <br>
        Heeft u (mogelijke) fouten gevonden of heeft u opmerkingen? Contacteer dan <span class="font-weight-bold">ItsJulian#8581</span>.</p>
    <br>
    <?php if(hasPerms($_SESSION['org'],$_SESSION['user_id'], 'page.home.stats')){ ?>
        <?php require_once './includes/modules/org_home_stats.php'; ?>
    <?php }else{ ?>
        <img src="/includes/img/quick.jpg">
    <?php } ?>
<?php } ?>
