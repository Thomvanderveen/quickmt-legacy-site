<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($request[2], $request[3]) && $request[3] = "delete"){
    if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.notities.remove")){
        $note = getNotitie($request[2]);
        if(!empty($note)){
            if($note['deletedOn'] == 0){
                deleteNotitie($request[2], $_SESSION['user_id']);
                $org = getOrganisation($_SESSION['org']);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") een notitie verwijderd");
                $_SESSION['notitie_deleted'] = 'true';
            }
        }
    }elseif(!isset($_SESSION['notitie_deleted'])){
        $_SESSION['notitie_deleted'] = 'false';
    }
}
?>

<?php require_once './includes/modules/org_notities_header.php'; ?>
<?php require_once './includes/modules/org_notities_tabel.php'; ?>
