<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($request[3]) && $request[3] == "checked"){
    voorraadChecked($_SESSION['user_id'], $_SESSION['org']);
    echo '<script>window.location.href = "/org/overzicht/voorraad/";</script>';
    return;
}
if(isset($_POST['vooraad_product'])){
    if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.voorraad.manage")){
        $productid = $_POST['vooraad_product'];
        if(isset($_POST['addVoorraad']) && is_numeric($_POST['addVoorraad']) && $_POST['addVoorraad'] >= 1){
            if(orgHasProduct($_SESSION['org'],$productid)){
                addVoorraad($_SESSION['org'], $productid, $_POST['addVoorraad']);
                $org = getOrganisation($_SESSION['org']);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") de vooraad van het product ".getProductByID($productid)['name']." bijgewerkt met +" . $_POST['addVoorraad']);
                $_SESSION['vooraad_changed'] = true;
            }
        }elseif(isset($_POST['removeVoorraad']) && is_numeric($_POST['removeVoorraad']) && $_POST['removeVoorraad'] >= 1){
            if(orgHasProduct($_SESSION['org'],$productid)){
                removeVoorraad($_SESSION['org'], $productid, $_POST['removeVoorraad']);
                $org = getOrganisation($_SESSION['org']);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") de vooraad van het product ".getProductByID($productid)['name']." bijgewerkt met -" . $_POST['removeVoorraad']);
                $_SESSION['vooraad_changed'] = true;
            }
        }
    }

    if(!isset($_SESSION['vooraad_changed'])){
        $_SESSION['vooraad_changed'] = 'false';
    }
}
?>
<?php require_once './includes/modules/org_overzicht_voorraad_header.php'; ?>
<?php require_once './includes/modules/org_overzicht_voorraad_table.php'; ?>