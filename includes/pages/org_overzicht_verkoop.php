<?php
/*
 * Copyright 2020 Julian Meurer
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

if(isset($request[3], $request[4]) && $request[4] = "dell"){
    if(hasPerms($_SESSION['org'],$_SESSION['user_id'], "page.overzicht.verkopen.manage")){
        $verkoop = getVerkoop($_SESSION['org'], $request[3]);
        if(!empty($verkoop)){
            if($verkoop[0]['time'] > getOrganisation($_SESSION['org'])['lastLoonReset']){
                $producten = json_decode($verkoop[0]['producten'], true);
                foreach ($producten as $productid => $aantal){
                    addVoorraad($_SESSION['org'], $productid, $aantal);
                }
                deleteVerkoop($_SESSION['org'], $request[3]);
                $org = getOrganisation($_SESSION['org']);
                addLog($_SESSION['user_id'], "Succesvolle voor de organisatie ". $org["name"] . " (".$org['location'].") een verkoop verwijderd");
                $_SESSION['verkoop_deleted'] = 'true';
            }elseif(!isset($_SESSION['verkoop_deleted'])){
                $_SESSION['verkoop_deleted'] = 'false';
            }
        }
    }elseif(!isset($_SESSION['verkoop_deleted'])){
        $_SESSION['verkoop_deleted'] = 'false';
    }
}
?>

<?php require_once './includes/modules/org_overzicht_verkopen_header.php'; ?>
<?php require_once './includes/modules/org_overzicht_verkopen_tabel.php'; ?>